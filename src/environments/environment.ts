// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyBwIW6s2Ccyee6H4Xoss_j4Zoni4VBd0i4',
    authDomain: 'controle-flavia.firebaseapp.com',
    databaseURL: 'https://controle-flavia.firebaseio.com',
    projectId: 'controle-flavia',
    storageBucket: 'controle-flavia.appspot.com',
    messagingSenderId: '245331072299',
    appId: '1:245331072299:web:60a5c85630afba70eda16e',
    measurementId: 'G-18VG8RNYXL'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
